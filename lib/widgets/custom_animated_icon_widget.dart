import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomAnimatedIconWidget extends StatefulWidget {
  final Duration duration;
  final Color color;
  final Color strokeColor;
  final double size;
  CustomAnimatedIconWidget({
    @required this.duration,
    this.size = 25.0,
    this.color,
    this.strokeColor,
  }) : assert(duration != null);
  @override
  _CustomAnimatedIconWidgetState createState() => _CustomAnimatedIconWidgetState();
}
class _CustomAnimatedIconWidgetState extends State<CustomAnimatedIconWidget> with TickerProviderStateMixin {
  AnimationController animationController;
  AnimationController animationTwoController;
  bool realtimeMove = false;
  Tween tween;
  Animation animation;
  @override
  void initState() {
    animationController = AnimationController(duration: widget.duration, vsync: this, value: 0.0);
    animationTwoController = AnimationController(duration: widget.duration, vsync: this, value: 0.0);
    animationController.addListener(_firstControllerUpdateListener);
    animationTwoController.addListener(_secondControllerUpdateListener);
    super.initState();
  }
  @override
  void dispose() {
    animationController.removeListener(_firstControllerUpdateListener);
    animationTwoController.removeListener(_secondControllerUpdateListener);
    animationController.dispose();
    animationTwoController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Transform.rotate(
        angle: -(pi / (2 / (1 - animationController.value))),
        child: Container(
          width: widget.size,
          height: widget.size,
          child: GestureDetector(
            onTap: _buttonTap,
            child: Container(
              color: Colors.transparent,
              child: CustomPaint(
                foregroundPainter: CustomIconPaint(
                  color: widget.color,
                  strokeColor: widget.strokeColor,
                  sizeIcon: widget.size,
                  value: animationTwoController.value,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
  void _buttonTap() {
    if (animationController.value == 0) {
      animationController.forward();
    } else {
      animationController.reverse();
      animationTwoController.reverse();
    }
  }
  void _firstControllerUpdateListener() {
    if (animationController.value == 1) {
      if (animationTwoController.value != 1) {
        animationTwoController.forward();
      }
    } else {
      animationTwoController.reverse();
    }
    setState(() {});
  }
  void _secondControllerUpdateListener() => setState(() {});
}


class CustomIconPaint extends CustomPainter {
  final double value;
  final double sizeIcon;
  final Color color;
  final Color strokeColor;
  CustomIconPaint({
    @required this.value,
    @required this.sizeIcon,
    @required this.color,
    @required this.strokeColor,
  });
  void _drawPath(Canvas canvas, Paint paint, Paint paintStroke) {
    canvas.save();
    Path path = Path();
    canvas.restore();
    path.addPolygon(
      [
        Offset(0, 0),
        Offset(sizeIcon, (sizeIcon / 2) * value),
        Offset(sizeIcon, (sizeIcon / 2 - ((sizeIcon / 8) * (1 - value)))),
        Offset(0, (sizeIcon / 2 - ((sizeIcon / 8) * (1 - value)))),
      ],
      true,
    );
    path.addPolygon(
      [
        Offset(0, sizeIcon),
        Offset(sizeIcon, sizeIcon - (sizeIcon / 2) * value),
        Offset(sizeIcon, (sizeIcon / 2 + ((sizeIcon / 8) * (1 - value)))),
        Offset(0, (sizeIcon / 2 + ((sizeIcon / 8) * (1 - value)))),
      ],
      true,
    );
    canvas.drawPath(path, paintStroke);
    canvas.drawPath(path, paint);
  }
  @override
  void paint(Canvas canvas, Size size) {
    size = Size(sizeIcon, sizeIcon);
    Paint paint = Paint()
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill
      ..color = color ?? Colors.black;
    Paint paintStroke = Paint()
      ..strokeCap = StrokeCap.square
      ..strokeJoin = StrokeJoin.round
      ..strokeWidth = sizeIcon /10
      ..style = PaintingStyle.stroke
      ..color = strokeColor ?? Colors.black;
    _drawPath(canvas, paint, paintStroke);
  }
  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}