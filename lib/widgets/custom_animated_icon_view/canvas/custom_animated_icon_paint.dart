import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomIconViewPaint extends CustomPainter {
  final double value;
  final double value2;
  final double sizeIcon;
  final Color color;
  final Color strokeColor;

  CustomIconViewPaint({
    @required this.value,
    @required this.value2,
    @required this.sizeIcon,
    @required this.color,
    @required this.strokeColor,
  });

  void _drawPath(Canvas canvas, Paint paint, Paint paintStroke) {
    canvas.save();
    Path path = Path();
    canvas.restore();

    double reverseValue = (1 - value);
    double dynamicSize = (sizeIcon * 0.66) + ((sizeIcon * 0.33) * reverseValue);
    double space = ((dynamicSize / 8) * 1);
    double padding = space * value;
    double paddingReverse = space * reverseValue;
    double size = (dynamicSize / 8) * value;
    double topPadding = (((dynamicSize / 2) - size) - space) - padding;
    double topDown = (sizeIcon - size) * value;
    double topDownCenter = (sizeIcon - size) / 1.5 * value;
    double topUpCenter = (space * 3.8) * value;
    double topUp = (space * 0.2) * value;
    double rotate = dynamicSize * (value > 0.5 ? 1 - value : value);
    double rotate2 = dynamicSize * (value > 0.5 ? 1 - value : value);

    double withLine = ((size * 4) * value2);
    double heightLine = ((sizeIcon) * value2);

    ///Lines
    path.addPolygon(
      [
        Offset(0 - padding + withLine, topDown * value),
        Offset(topPadding + heightLine, topDown * value),
        Offset(topPadding + heightLine, ((dynamicSize / 2 - size) - space) + topDown * value),
        Offset(0 - padding + withLine, ((dynamicSize / 2 - size) - (dynamicSize / 8)) + topDown * value),
      ],
      true,
    );
    path.addPolygon(
      [
        Offset(((dynamicSize / 2) * reverseValue - padding + space * reverseValue) + withLine, topDownCenter * value),
        Offset((dynamicSize * reverseValue) + (size * value) + heightLine, topDownCenter * value),
        Offset((dynamicSize * reverseValue) + (size * value) + heightLine,
            ((dynamicSize / 2) - size - space) + topDownCenter * value),
        Offset(((dynamicSize / 2) * reverseValue - padding + space * reverseValue) + withLine,
            ((dynamicSize / 2) - size - space) + topDownCenter * value),
      ],
      true,
    );
    path.addPolygon(
      [
        Offset(0 - padding + rotate + withLine, ((topUpCenter)) + (dynamicSize / 2 * reverseValue + paddingReverse)),
        Offset(topPadding + rotate + heightLine, topUpCenter + (dynamicSize / 2 * reverseValue + paddingReverse)),
        Offset(topPadding + rotate + heightLine,
            topUpCenter + size * 2 + (paddingReverse * 3) + (dynamicSize / 2 * reverseValue + paddingReverse)),
        Offset(0 - padding + rotate + withLine,
            topUpCenter + size * 2 + (paddingReverse * 3) + (dynamicSize / 2 * reverseValue + paddingReverse)),
      ],
      true,
    );
    path.addPolygon(
      [
        Offset(((dynamicSize / 2) * reverseValue - padding + space * reverseValue) + (rotate2 / 3) + withLine,
            ((topUp)) + (dynamicSize / 2 * reverseValue + paddingReverse) - (rotate2 / 3)),
        Offset((dynamicSize * reverseValue) + (size * value) + (rotate2 / 3) + heightLine,
            topUp + (dynamicSize / 2 * reverseValue + paddingReverse) - (rotate2 / 3)),
        Offset(
            (dynamicSize * reverseValue) + (size * value) + (rotate2 / 3) + heightLine,
            topUp +
                size * 2 +
                (paddingReverse * 3) +
                (dynamicSize / 2 * reverseValue + paddingReverse) -
                (rotate2 / 3)),
        Offset(
            ((dynamicSize / 2) * reverseValue - padding + space * reverseValue) + (rotate2 / 3) + withLine,
            topUp +
                size * 2 +
                (paddingReverse * 3) +
                (dynamicSize / 2 * reverseValue + paddingReverse) -
                (rotate2 / 3)),
      ],
      true,
    );

    ///Rectangle
    path.addPolygon(
      [
        Offset(0 - padding, topDown * value),
        Offset(topPadding, topDown * value),
        Offset(topPadding, ((dynamicSize / 2 - size) - space) + topDown * value),
        Offset(0 - padding, ((dynamicSize / 2 - size) - (dynamicSize / 8)) + topDown * value),
      ],
      true,
    );
    path.addPolygon(
      [
        Offset((dynamicSize / 2) * reverseValue - padding + space * reverseValue, topDownCenter * value),
        Offset((dynamicSize * reverseValue) + (size * value), topDownCenter * value),
        Offset(
            (dynamicSize * reverseValue) + (size * value), ((dynamicSize / 2) - size - space) + topDownCenter * value),
        Offset((dynamicSize / 2) * reverseValue - padding + space * reverseValue,
            ((dynamicSize / 2) - size - space) + topDownCenter * value),
      ],
      true,
    );
    path.addPolygon(
      [
        Offset(0 - padding + rotate, ((topUpCenter)) + (dynamicSize / 2 * reverseValue + paddingReverse)),
        Offset(topPadding + rotate, topUpCenter + (dynamicSize / 2 * reverseValue + paddingReverse)),
        Offset(topPadding + rotate,
            topUpCenter + size * 2 + (paddingReverse * 3) + (dynamicSize / 2 * reverseValue + paddingReverse)),
        Offset(0 - padding + rotate,
            topUpCenter + size * 2 + (paddingReverse * 3) + (dynamicSize / 2 * reverseValue + paddingReverse)),
      ],
      true,
    );
    path.addPolygon(
      [
        Offset(((dynamicSize / 2) * reverseValue - padding + space * reverseValue) + (rotate2 / 3),
            ((topUp)) + (dynamicSize / 2 * reverseValue + paddingReverse) - (rotate2 / 3)),
        Offset((dynamicSize * reverseValue) + (size * value) + (rotate2 / 3),
            topUp + (dynamicSize / 2 * reverseValue + paddingReverse) - (rotate2 / 3)),
        Offset(
            (dynamicSize * reverseValue) + (size * value) + (rotate2 / 3),
            topUp +
                size * 2 +
                (paddingReverse * 3) +
                (dynamicSize / 2 * reverseValue + paddingReverse) -
                (rotate2 / 3)),
        Offset(
            ((dynamicSize / 2) * reverseValue - padding + space * reverseValue) + (rotate2 / 3),
            topUp +
                size * 2 +
                (paddingReverse * 3) +
                (dynamicSize / 2 * reverseValue + paddingReverse) -
                (rotate2 / 3)),
      ],
      true,
    );
    canvas.drawPath(path, paintStroke);
    canvas.drawPath(path, paint);

  }

  @override
  void paint(Canvas canvas, Size size) {
    size = Size(sizeIcon, sizeIcon);

    Paint paint = Paint()
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill
      ..color = color ?? Colors.black;

    Paint paintStroke = Paint()
      ..strokeCap = StrokeCap.square
      ..strokeJoin = StrokeJoin.round
      ..strokeWidth = sizeIcon / (10 + (20 * value))
      ..style = PaintingStyle.stroke
      ..color = strokeColor ?? Colors.black;
    _drawPath(canvas, paint, paintStroke);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
