import 'dart:math';

import 'package:canvas_example/widgets/custom_animated_icon_view/canvas/custom_animated_icon_paint.dart';
import 'package:flutter/material.dart';


class CustomAnimatedIconViewWidget extends StatefulWidget {
  final double size;
  final Color color;
  final Color strokeColor;
  final Duration duration;
  final void Function() onTap;

  CustomAnimatedIconViewWidget({
    @required this.duration,
    @required this.onTap,
    this.size = 25.0,
    this.color,
    this.strokeColor,
  }) : assert(duration != null && onTap != null);

  @override
  _CustomAnimatedIconViewWidgetState createState() => _CustomAnimatedIconViewWidgetState();
}

class _CustomAnimatedIconViewWidgetState extends State<CustomAnimatedIconViewWidget> with TickerProviderStateMixin {
  AnimationController animationController;
  AnimationController animationTwoController;
  AnimationController animationThreeController;
  bool realtimeMove = false;

  Tween tween;
  Animation animation;

  @override
  void initState() {
    animationController = AnimationController(duration: widget.duration, vsync: this, value: 0.0);
    animationTwoController = AnimationController(duration: widget.duration, vsync: this, value: 0.0);
    animationThreeController = AnimationController(duration: widget.duration, vsync: this, value: 0.0);

    animationController.addListener(_firstControllerUpdateListener);
    animationTwoController.addListener(_secondControllerUpdateListener);
    animationThreeController.addListener(_threeControllerUpdateListener);
    super.initState();
  }

  @override
  void dispose() {
    animationController.removeListener(_firstControllerUpdateListener);
    animationTwoController.removeListener(_secondControllerUpdateListener);
    animationThreeController.removeListener(_threeControllerUpdateListener);

    animationController.dispose();
    animationTwoController.dispose();
    animationThreeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: InkWell(
        onTap: _buttonTap,
        child: Container(
          width: widget.size * 1.5,
          height: widget.size * 1.5,
          alignment: Alignment(0.2, 0.0),
          child: Transform.rotate(
            angle: -(pi / (2 / (1 - animationController.value))),
            child: Container(
              width: widget.size,
              height: widget.size,
              child: CustomPaint(
                foregroundPainter: CustomIconViewPaint(
                  color: widget.color,
                  strokeColor: widget.strokeColor,
                  sizeIcon: widget.size * 0.9,
                  value: animationTwoController.value,
                  value2: animationThreeController.value,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _buttonTap() {
    widget.onTap();
    if (animationController.value == 0) {
      animationController.forward();
    } else {
      animationThreeController.reverse();
    }
  }

  void _firstControllerUpdateListener() {
    if (animationController.value == 1) {
      if (animationTwoController.value != 1) {
        animationTwoController.forward();
      }
    }
    setState(() {});
  }

  void _secondControllerUpdateListener() {
    if (animationTwoController.value == 1) {
      animationThreeController.forward();
    }
    setState(() {});
  }

  void _threeControllerUpdateListener() {
    if (animationThreeController.value == 0) {
      animationTwoController.reverse();
      animationController.reverse();
    }
    setState(() {});
  }
}
