import 'package:flutter/material.dart';

class CircleWaveRoute extends StatefulWidget {
  final double dx;
  final double dy;

  CircleWaveRoute({
    @required this.dx,
    @required this.dy,
  });

  @override
  _CircleWaveRouteState createState() => _CircleWaveRouteState();
}

class _CircleWaveRouteState extends State<CircleWaveRoute> with SingleTickerProviderStateMixin {
  double waveRadius = 0.0;
  double waveGap = 100.0;
  Animation<double> _animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(duration: Duration(milliseconds: 1000), vsync: this);

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    _animation = Tween(begin: 0.0, end: waveGap).animate(controller)
      ..addListener(() {
        setState(() {
          waveRadius = _animation.value;
        });
      });
    return Opacity(
      opacity: 1 - (_animation.value / waveGap),
      child: CustomPaint(
        size: Size(double.infinity, double.infinity),
        painter: CircleWavePainter(waveRadius: waveRadius, dx: widget.dx, dy: widget.dy),
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class CircleWavePainter extends CustomPainter {
  final double waveRadius;
  final double dx;
  final double dy;
  var wavePaint;

  CircleWavePainter({this.waveRadius, this.dx, this.dy}) {
    wavePaint = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5.0
      ..isAntiAlias = true;
  }

  @override
  void paint(Canvas canvas, Size size) {
    double centerX = dx;
    double centerY = dy;

    var currentRadius = waveRadius;
    canvas.drawCircle(Offset(centerX, centerY), currentRadius, wavePaint);
    currentRadius += 100.0;
  }

  @override
  bool shouldRepaint(CircleWavePainter oldDelegate) {
    return true;
  }
}
