import 'package:flutter/material.dart';

class MainGridItem extends StatelessWidget {
  final void Function() onTap;
  final String title;
  final IconData icon;

  MainGridItem({
    @required this.icon,
    @required this.onTap,
    @required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: InkWell(
        borderRadius: BorderRadius.circular(16.0),
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white24,
              borderRadius: BorderRadius.circular(16.0),
              border: Border.all(color: Colors.black, width: 3.0)),
          child: GridTile(
            child: Icon(icon, size: 40),
            footer: Container(
              height: 40.0,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.black54,
                borderRadius: BorderRadius.vertical(bottom: Radius.circular(12.0)),
              ),
              child: Text(
                title,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 18, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
