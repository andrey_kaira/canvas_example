import 'package:canvas_example/main_grid_item.dart';
import 'package:canvas_example/pages/text_canvas_page.dart';
import 'package:canvas_example/pages/view_animated_icon.dart';
import 'package:canvas_example/pages/wave_animation_page.dart';
import 'package:flutter/material.dart';

import 'animated_icon_play_page.dart';
import 'path_page.dart';


class HomePage extends StatefulWidget {
  static String kRoute = '';

  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.title)),
      body: GridView.count(
        padding: const EdgeInsets.all(8.0),
        crossAxisCount: 2,
        children: <Widget>[
          MainGridItem(
            icon: Icons.gesture,
            title: 'Path',
            onTap: () => Navigator.of(context).pushNamed(PathPage.kRoute),
          ),
          MainGridItem(
            icon: Icons.text_fields,
            title: 'Text canvas',
            onTap: () => Navigator.of(context).pushNamed(TextCanvasPage.kRoute),
          ),
          MainGridItem(
            icon: Icons.play_arrow,
            title: 'Play icon',
            onTap: () => Navigator.of(context).pushNamed(AnimatedIconPlayPage.kRoute),
          ),
          MainGridItem(
            icon: Icons.format_list_bulleted,
            title: 'View icon',
            onTap: () => Navigator.of(context).pushNamed(ViewAnimatedIcon.kRoute),
          ),
          MainGridItem(
            icon: Icons.opacity,
            title: 'Wave Circle',
            onTap: () => Navigator.of(context).pushNamed(WaveAnimationPage.kRoute),
          ),
        ],
      ),
    );
  }
}

