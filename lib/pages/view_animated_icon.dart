import 'package:canvas_example/widgets/custom_animated_icon_view/widgets/custom_animated_icon_widgets.dart';
import 'package:flutter/material.dart';

class ViewAnimatedIcon extends StatelessWidget {
  static String kRoute = '/view-animated-icon';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomAnimatedIconViewWidget(
        onTap: () {},
        size: 60.0,
        strokeColor: Colors.red,
        color: Colors.orangeAccent,
        duration: Duration(milliseconds: 400),
      ),
    );
  }
}
