import 'dart:ui';

import 'package:canvas_example/widgets/circular_wave_animation.dart';
import 'package:flutter/material.dart';

class WaveAnimationPage extends StatefulWidget {
  static String kRoute = '/wave-page';

  @override
  _WaveAnimationPageState createState() => _WaveAnimationPageState();
}

class _WaveAnimationPageState extends State<WaveAnimationPage> {
  final List<Offset> offsets = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: GestureDetector(
        onTapDown: (TapDownDetails tap) => setState(() => offsets.add(tap.globalPosition)),
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.black,
          child: Stack(
            children: <Widget>[
              for (Offset offset in offsets) CircleWaveRoute(dx: offset.dx, dy: offset.dy),
              Positioned.fill(
                child: BackdropFilter(
                  filter: ImageFilter.blur(
                    sigmaX: 3.0,
                    sigmaY: 3.0,
                  ),
                  child: Container(
                    color: Colors.transparent,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
