import 'package:canvas_example/widgets/custom_animated_icon_widget.dart';
import 'package:flutter/material.dart';

class AnimatedIconPlayPage extends StatelessWidget {
  static String kRoute = '/animated-icon-plage';

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: CustomAnimatedIconWidget(
          duration: Duration(milliseconds: 400),
          color: Colors.lightGreen,
          strokeColor: Colors.green,
          size: 50.0,
        ),
      )
    );
  }
}
