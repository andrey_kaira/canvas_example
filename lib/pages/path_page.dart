import 'package:flutter/material.dart';

class PathPage extends StatelessWidget {
  static String kRoute = '/path-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomPaint(
        size: Size(double.infinity, double.infinity),
        painter: PathPainter(),
      ),
    );
  }
}

class PathPainter extends CustomPainter {
  var onePaint;
  var twoPaint;

  PathPainter() {
    onePaint = Paint()
      ..color = Colors.green
      ..style = PaintingStyle.stroke
      ..strokeWidth = 15.0
      ..isAntiAlias = true;
    twoPaint = Paint()
      ..color = Colors.lightGreenAccent
      ..style = PaintingStyle.fill
      ..strokeWidth = 5.0
      ..isAntiAlias = true;
  }

  @override
  void paint(Canvas canvas, Size size) {
    double centerX = size.width / 2;
    double centerY = size.height / 2;

    Path path = Path();

    path.addPolygon([
      Offset(centerX - 200, centerY - 200),
      Offset(centerX + 200, centerY - 200),
      Offset(centerX - 200, centerY + 200),
      Offset(centerX + 200, centerY + 200),
    ], true);
    canvas.drawPath(path, twoPaint);

    path.addPolygon([
      Offset(centerX - 100, centerY - 100),
      Offset(centerX + 100, centerY - 100),
      Offset(centerX - 100, centerY + 100),
      Offset(centerX + 100, centerY + 100),
    ], true);
    canvas.drawPath(path, onePaint);
  }

  @override
  bool shouldRepaint(PathPainter oldDelegate) {
    return true;
  }
}
