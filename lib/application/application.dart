import 'package:canvas_example/pages/animated_icon_play_page.dart';
import 'package:canvas_example/pages/home_page.dart';
import 'package:canvas_example/pages/path_page.dart';
import 'package:canvas_example/pages/text_canvas_page.dart';
import 'package:canvas_example/pages/view_animated_icon.dart';
import 'package:canvas_example/pages/wave_animation_page.dart';
import 'package:flutter/material.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      home: HomePage(title: 'Flutter Demo Home Page'),
      routes: {
        HomePage.kRoute: (context) => HomePage(),
        WaveAnimationPage.kRoute: (context) => WaveAnimationPage(),
        AnimatedIconPlayPage.kRoute: (context) => AnimatedIconPlayPage(),
        PathPage.kRoute: (context) => PathPage(),
        TextCanvasPage.kRoute: (context) => TextCanvasPage(),
        ViewAnimatedIcon.kRoute: (context) => ViewAnimatedIcon(),
      },
    );
  }
}
